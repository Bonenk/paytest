(function(window, undefined) {
  'use strict';

  /*
  NOTE:
  ------
  PLACE HERE YOUR OWN JAVASCRIPT CODE IF NEEDED
  WE WILL RELEASE FUTURE UPDATES SO IN ORDER TO NOT OVERWRITE YOUR JAVASCRIPT CODE PLEASE CONSIDER WRITING YOUR SCRIPT HERE.  */

  // $( document ).ready(function() {
  //   if(localStorage.getItem('theme') === "dark"){
  //     document.getElementById("body").classList.add('dark-layout');
  //     document.getElementById("menu-list").classList.add('menu-dark');
  //   }
  //   else{
  //     document.getElementById("body").classList.remove('dark-layout');
  //     document.getElementById("menu-list").classList.remove('menu-dark');
  //   }
  // });
  
  const darkswitch = document.getElementById('darkswitch');

  darkswitch.addEventListener('change', () => {
      document.getElementById("body").classList.toggle('dark-layout');
      document.getElementById("menu-list").classList.toggle('menu-dark');
  });

})(window);